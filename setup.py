from setuptools import setup

setup(
    name='genpssi',
    version="0.1",
    description='Generate PSSI documents',
    author='Joris Picot (LIP)',
    packages=['genpssi'],
    dependencies=['jinja2'],
    include_package_data=True,
)
