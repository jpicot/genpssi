\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{argumentaire}

%% Article options
\DeclareOption{handout}{
  \PassOptionsToClass{\CurrentOption}{report}
}

%% Process given options
\ProcessOptions\relax

%% Load base
\LoadClass{report}

\DeclareUnicodeCharacter{207B}{$^-$}

% \usepackage[utf8]{inputenc}
% \usepackage{codepoints}
% 
% \usepackage{fourier}
% \usepackage{microtype}
% \usepackage[english, french]{babel}
% 
% 
% \usepackage{beamertricks}
% 
% 
% \usefonttheme{serif}
% \setbeamertemplate{navigation symbols}{}
% \setbeamercolor{footline}{parent=structure,fg=structure!50!white}
% \setbeamerfont{footline}{size=\footnotesize}
% 
% \setbeamertemplate{footline}
% {%
%   \leavevmode%
%   \hfill%
%   {\bfseries\insertframenumber/\inserttotalframenumber}\kern1em\vskip2pt%
% }
% 
% 
% \definecolor{lip}{HTML}{b00040}
% \setbeamercolor{structure}{fg=lip}
