"""Genpssi command-line.

Dispatch between subcommands.

"""
import argparse
import sys

from . import actionplan
from . import dashboard


def main():
    """command-line entry point"""

    parser = argparse.ArgumentParser(description="")
    subparsers = parser.add_subparsers(dest="action")

    subparser = subparsers.add_parser("actionplan")
    actionplan.add_arguments(subparser)

    subparser = subparsers.add_parser("dashboard")
    dashboard.add_arguments(subparser)

    args = parser.parse_args()

    if args.action == "actionplan":
        return actionplan.main(args)

    if args.action == "dashboard":
        return dashboard.main(args)

    return -1


if __name__ == "__main__":
    sys.exit( main() )
