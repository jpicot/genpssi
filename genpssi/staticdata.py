"""Load and provide static data on the PSSI"""

import functools
import os.path

import jinja2
import yaml

from . import STATIC_DIR


_JINJA_ENV = None
_VALID_NAMES = [
    "actionplan.md",
    "dashboard.html",
    "rule_descr",       ## Description for each rule
    "rule_level",       ## Rule levels
    "rule_struct",      ## dict-tree structure of rules {group: [rule, …], …}
    "rule_titles",      ## Names for each rule and rule category
    ]


@functools.cache
def get(fmt, name):
    "get static data in file"

    ## Prevent arbitrary filenames.
    if name not in _VALID_NAMES:
        raise RuntimeError(f"invalid static data: {name}")

    if fmt.lower() == "yaml":
        filename = os.path.join(STATIC_DIR, name + "." + fmt)

        with open(filename) as stream:
            return yaml.load(stream, Loader=yaml.Loader)

    elif fmt.lower() == "jinja":
        filename = name + "." + fmt
        
        global _JINJA_ENV
        if _JINJA_ENV is None:
            _JINJA_ENV = jinja2.Environment(loader=jinja2.FileSystemLoader(STATIC_DIR), trim_blocks=True)

        return _JINJA_ENV.get_template(filename)

    else:
        raise RuntimeError("no getter for this static file format")


def title(ruleid):
    "get rule title"
    titles = get("yaml", "rule_titles")

    return titles.get(ruleid, "")


def descr(ruleid):
    "get rule description"
    descrs = get("yaml", "rule_descr")

    return descrs.get(ruleid, "")


def level(ruleid):
    "get rule level"
    lvls = get("yaml", "rule_level")

    return lvls.get(ruleid, 999)

