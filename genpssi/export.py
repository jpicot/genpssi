import locale

from subprocess import PIPE, Popen

from . import STATIC_DIR


FORMATS = [
    "markdown",
    "xelatex",
    "xelatex-src",
]


def export(fmt, arguments, output=None):
    fmt = fmt.lower()

    if fmt == "html":
        print(arguments)

    elif fmt == "markdown":
        print(arguments)

    elif fmt == "xelatex-src":
        print(_xelatex_src(arguments))

    elif fmt == "xelatex":

        if output is None:
            print("An output file is required for this format.")
            return -1

        with open(output, "wb") as f:
            arguments = _xelatex(arguments)
            f.write(arguments)

    else:
        ## Should never happen
        return -1

    return 0


def _xelatex_src(md_input):
    process = ["pandoc",
        f"--data-dir={STATIC_DIR}",
        "--to=latex", "--standalone"]
    process = Popen(process, stdin=PIPE, stdout=PIPE)
    md_input = bytes(md_input, "utf-8")
    (out, _) = process.communicate(md_input)
    return str(out, locale.getpreferredencoding())


def _xelatex(md_input):
    process = ["pandoc",
        f"--data-dir={STATIC_DIR}",
        "--to=pdf", "--pdf-engine=xelatex", "--standalone"]
    process = Popen(process, stdin=PIPE, stdout=PIPE)
    md_input = bytes(md_input, "utf-8")
    (out, _) = process.communicate(md_input)
    return out
