"Construction d'un dashboard"

import yaml

from . import export
from . import staticdata


def _make(pssi):

    rule_struct = staticdata.get("yaml", "rule_struct")
    template = staticdata.get("jinja", "dashboard.html")

    ## Group argumentation data in a list of sections (POL, ORG, …).

    sections = []

    for group_id, rule_group in rule_struct.items():
        sections.append({})

        ## Add data to current section.

        sections[-1]["id"] = group_id
        sections[-1]["title"] = staticdata.title(group_id)
        try:
            sections[-1]["intro"] = pssi.get(group_id).get("intro")
        except AttributeError:
            sections[-1]["intro"] = None

        ## "body" contains subsections, that are argumentation for each rule.

        sections[-1]["body"] = []

        for rule_id in rule_group:
            rule_id = "-".join([group_id, rule_id])

            ## Collect rule arguments, and complete information.

            data = pssi.get(rule_id, {})
            data["id"] = rule_id
            #data.setdefault("date", "")

            sections[-1]["body"].append(data)

    return template.render(sections=sections, staticdata=staticdata)

    return template.render()


def add_arguments(parser):
    """add arguments for argumentaire subcommand"""

    parser.description = """\
        Generate a dashboard.
        """

    parser.add_argument("pssi", metavar="pssi.yaml",
        help="state description of your PSSI")


def main(args):
    """entry point"""

    with open(args.pssi) as f:
        pssi = yaml.load(f, Loader=yaml.Loader)

    ## Generate Argumentaire in Markdown
    dashboard = _make(pssi)

    return export.export("html", dashboard, None)
